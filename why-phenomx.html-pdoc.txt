 

[Original on Transparent.png]

-   
    Home

-   
    Blog

-   
    Team

-   
    Contact

-   
    News

-   More

Use tab to navigate through the menu items.

-   All Posts

Search

Log in / Sign up

-   
    []Dr. Colleen Fogarty Draper RD, PhD

-   

-   -   4 days ago

    -   

    -   1 min read

Why PhenomX?

Perimenopause is the transitional time to menopause that can last 8-10
years before menstruation ceases. It is most common to start this
transition in her 40s and end in her 50s. During this time, she may
experience new and different health symptoms that reflect her transition
to a new hormonal equilibrium. It is officially over once she has 12
months without a period but she may continue to have symptoms a few
years past this time. Everyone’s experience during this life transition
is unique. Women entering the stage of peri-menopause are in the process
of creation of a new life rhythm. This is why it is sometimes
uncomfortable. The more she is open to change, the better the outcome.
However, this does not mean the journey is easy. The outcome is better
if she is open to the challenges, discomforts and unexpected solutions.
The truth is a full investment in the journey guarantees discomfort.
This discomfort comes with an upside. It is an opportunity to establish
a deep knowing of self and a strong comfort with life. Our PhenomX
platform is for women who wish to know more about their personal
experience during this life transition and how to treat their health
symptoms using nutrition therapies for a healthy and empowered aging
experience.

[]

0 views0 comments

Post not marked as liked

 

Sign up for PhenomX Updates

Submit

Thanks for submitting!

+41 79 866 82 55

-   [Facebook]
-   [Twitter]
-   [LinkedIn]

©2022 by PhenomX Health. Proudly created with Wix.com

 
