::: {#SITE_CONTAINER}
::: {#main_MF}
::: {#SCROLL_TO_TOP .qhwIj .ignore-focus tabindex="-1" role="region" aria-label="top of page"}
 
:::

::: {#BACKGROUND_GROUP}
::: {#pageBackground_ghb6v ._2v3yk media-height-override-type="" media-position-override="false"}
::: {#bgLayers_pageBackground_ghb6v ._3wnIc hook="bgLayers"}
::: {._3KzuS ._3SQN- testid="colorUnderlay"}
:::

::: {#bgMedia_pageBackground_ghb6v ._2GUhU}
:::

::: {._1eCSh testid="bgOverlay"}
:::
:::
:::
:::

::: {#site-root}
::: {#masterPage .mesh-layout}
::: {#SITE_HEADER .ZW5SX}
::: {._2o25M}
::: {._1nRb9}
:::
:::

::: {._38XI2}
::: {._2P6JN}
::: {._1dlfY}
:::
:::

::: {._1_UPn}
::: {mesh-id="SITE_HEADERinlineContent" testid="inline-content"}
::: {mesh-id="SITE_HEADERinlineContent-gridContainer" testid="mesh-container-content"}
::: {#comp-ky7dchdg .section ._3d64y}
::: {#bgLayers_comp-ky7dchdg ._3wnIc hook="bgLayers"}
::: {._3KzuS ._3SQN- testid="colorUnderlay"}
:::

::: {#bgMedia_comp-ky7dchdg ._2GUhU}
:::
:::

::: {._1uldx testid="columns"}
::: {#comp-ky7dchdh ._1vNJf}
::: {#bgLayers_comp-ky7dchdh ._3wnIc hook="bgLayers"}
::: {._3KzuS ._3SQN- testid="colorUnderlay"}
:::

::: {#bgMedia_comp-ky7dchdh ._2GUhU}
:::
:::

::: {mesh-id="comp-ky7dchdhinlineContent" testid="inline-content"}
::: {mesh-id="comp-ky7dchdhinlineContent-gridContainer" testid="mesh-container-content"}
::: {#comp-ky7dchdh1 .XUUsC ._2S73V title=""}
[![Original on
Transparent.png](Original%20on%20Transparent.png)](https://www.phenomxhealth.com){.xQ_iF}
:::
:::
:::
:::

::: {#comp-ky7dchdh2 ._1vNJf}
::: {#bgLayers_comp-ky7dchdh2 ._3wnIc hook="bgLayers"}
::: {._3KzuS ._3SQN- testid="colorUnderlay"}
:::

::: {#bgMedia_comp-ky7dchdh2 ._2GUhU}
:::
:::

::: {mesh-id="comp-ky7dchdh2inlineContent" testid="inline-content"}
::: {mesh-id="comp-ky7dchdh2inlineContent-gridContainer" testid="mesh-container-content"}
-   [[](https://www.phenomxhealth.com){._2DUrw}]{#comp-ky7dchkc0}
    ::: {._1u8sp}
    ::: {style="text-align:right"}
    Home
    :::
    :::

-   [[](https://www.phenomxhealth.com/blog){._2DUrw}]{#comp-ky7dchkc1}
    ::: {._1u8sp}
    ::: {style="text-align:right"}
    Blog
    :::
    :::

-   [[](https://www.phenomxhealth.com/team){._2DUrw}]{#comp-ky7dchkc2}
    ::: {._1u8sp}
    ::: {style="text-align:right"}
    Team
    :::
    :::

-   [[](https://www.phenomxhealth.com/contact){._2DUrw}]{#comp-ky7dchkc3}
    ::: {._1u8sp}
    ::: {style="text-align:right"}
    Contact
    :::
    :::

-   [[](https://www.phenomxhealth.com/news){._2DUrw}]{#comp-ky7dchkc4}
    ::: {._1u8sp}
    ::: {style="text-align:right"}
    News
    :::
    :::

-   ::: {#comp-ky7dchkc__more__}
    ::: {._2DUrw testid="linkElement" tabindex="0" aria-haspopup="false"}
    ::: {._1u8sp}
    ::: {style="text-align:right"}
    More
    :::
    :::
    :::
    :::

::: {#comp-ky7dchkcdropWrapper ._2vbYi dropalign="right" dropdown-shown="false"}
:::

::: {#comp-ky7dchkcnavContainer-hiddenA11ySubMenuIndication style="display:none"}
Use tab to navigate through the menu items.
:::
:::
:::
:::
:::
:::
:::
:::
:::
:::
:::

::: {#SITE_HEADER-placeholder}
:::

::: {#PAGES_CONTAINER role="main" tabindex="-1"}
::: {#SITE_PAGES}
::: {#ghb6v .Ry26q}
::: {._3CemL}
:::

::: {._3K7uv}
::: {#Containerghb6v ._1KV2M}
::: {mesh-id="Containerghb6vinlineContent" testid="inline-content"}
::: {mesh-id="Containerghb6vinlineContent-gridContainer" testid="mesh-container-content"}
::: {#comp-ky74cyyx .section ._3d64y}
::: {#bgLayers_comp-ky74cyyx ._3wnIc hook="bgLayers"}
::: {._3KzuS ._3SQN- testid="colorUnderlay"}
:::

::: {#bgMedia_comp-ky74cyyx ._2GUhU}
:::
:::

::: {._1uldx testid="columns"}
::: {#comp-ky74cyyx1 ._1vNJf}
::: {#bgLayers_comp-ky74cyyx1 ._3wnIc hook="bgLayers"}
::: {._3KzuS ._3SQN- testid="colorUnderlay"}
:::

::: {#bgMedia_comp-ky74cyyx1 ._2GUhU}
:::
:::

::: {mesh-id="comp-ky74cyyx1inlineContent" testid="inline-content"}
::: {mesh-id="comp-ky74cyyx1inlineContent-gridContainer" testid="mesh-container-content"}
:::
:::
:::
:::
:::

::: {#TPAMultiSection_ky74c1ea}
::: {.TPAMultiSection_ky74c1ea}
<div>

::: {.md .lt-lg .lt-xl .gt-xs .gt-s .gt-sm .gt-740 .gt-830 .gt-886 .gt-940 .w686-980 .lte-w980 .lte-banner-w1564 .lte-banner-w1425 .lte-banner-w1181 .lte-banner-w980 .lte-category-header-w1364 style="--root-width:980px" hook="responsive-listener"}
::: {._1CPxc .blog-background-color ._2QGTK .is-desktop .app-desktop}
<div>

::: {.uqkm_ .message hook="message"}
:::

</div>

<div>

</div>

::: {#new-blog-popover-root ._2bl1v}
:::

::: {#content-wrapper}
::: {._3Z5yN ._2euh2 ._2aryt .blog-header-background-color hook="blog-desktop-header-container"}
::: {._2S3x8 ._2PeVO}
-   [All Posts](https://www.phenomxhealth.com/blog){._2MzDA
    .blog-navigation-container-color .blog-navigation-container-font
    .blog-navigation-link-hover-color}

::: {._1c4oN}
::: {._d3Jy hook="search-input"}
::: {._2MHX- .search-input tabindex="-1"}
::: {tabindex="0" role="button" aria-label="Search"}
:::

::: {._1BO9x .blog-desktop-header-search-text-color .blog-desktop-header-search-font .k20aK}
Search
:::

::: {._3ybhJ .blog-desktop-header-search-border-background-color}
:::
:::
:::

[Log in / Sign up]{._5KG-s}
:::
:::
:::

::: {._2oYSu ._1Zq0x ._1uhkm .blog-post-page-font hook="post-page"}
::: {._1-0Rh}
<div>

</div>

<div>

<div>

<div>

::: {._3qxly}
::: {._10r66}
::: {._1AZWZ}
::: {._113Kr}
-   []{style="pointer-events:none"}
    ::: {.I6gS8 .blog-text-color .blog-link-hover-color .avatar .blog-post-text-color}
    [![](file.jpeg){._18Vq1 .fluid-avatar-image}]{._1NzhF .avatar-image
    i18n="[object Object]"}[Dr. Colleen Fogarty Draper RD, PhD]{.iYG_V
    .user-name ._4AzY3 title="Dr. Colleen Fogarty Draper RD, PhD"
    hook="user-name"}
    ::: {._2G7W_ aria-label="Writer" hook="badge"}
    :::
    :::

-   ::: {._2ybIo ._18oAz .blog-separator-background-color}
    :::

-   -   [4 days ago]{.post-metadata__date .time-ago title="4 days ago"
        hook="time-ago"}

    -   ::: {._2ybIo ._18oAz .blog-separator-background-color}
        :::

    -   [1 min read]{.post-metadata__readTime i18n="[object Object]"
        title="1 min read" hook="time-to-read"}

::: {._3Rw71 ._34iY0}
::: {._1Tzaw .mxwvP ._1ouGa .more-button}
:::
:::
:::

::: {._1KJEZ ._1Zljb tabindex="-1" hook="post-title"}
[[Why PhenomX?]{.blog-post-title-font .blog-post-title-color}]{.post-title__text .blog-post-title-font .blog-post-title-color} {#why-phenomx ._2zPFV ._3hhFM .blog-post-title-font .blog-post-title-color .blog-text-color .post-title .blog-hover-container-element-color ._3DFPj .blog-post-page-title-font hook="post-title"}
==============================================================================================================================
:::
:::

::: {._6SyeS hook="post-description"}
::: {.post-content__body}
::: {._2cDMG}
::: {._2cDMG}
::: {.bVAkx ._3777I ._1dQP3 style="--rce-text-color:rgb(24, 24, 24);--rce-opaque-background-color:rgb(255, 255, 255);--rce-divider-color:rgba(24, 24, 24, 0.2);--rce-active-divider-color:rgba(24, 24, 24, 0.5);--rce-highlighted-color:rgb(112, 158, 168);--rce-link-hashtag-color:rgb(112, 158, 168);--rce-mobile-font-size:16px;--rce-header-two-font-size:28px;--rce-header-three-font-size:22px;--rce-header-four-font-size:20px;--rce-header-five-font-size:18px;--rce-header-six-font-size:16px;--rce-mobile-header-two-font-size:24px;--rce-mobile-header-three-font-size:20px;--rce-mobile-header-four-font-size:20px;--rce-mobile-header-five-font-size:20px;--rce-mobile-header-six-font-size:20px;--rce-mobile-quotes-font-size:20px" rce-version="8.66.8"}
::: {.kvdbP ._2msPJ ._189VQ ._1O7aH dir="ltr" data-id="rich-content-viewer"}
::: {._1hN1O ._2S8BH ._3EPBy}
::: {type="first" hook="rcv-block-first"}
:::

[Perimenopause is the transitional time to menopause that can last 8-10
years before menstruation ceases. It is most common to start this
transition in her 40s and end in her 50s. During this time, she may
experience new and different health symptoms that reflect her transition
to a new hormonal equilibrium. It is officially over once she has 12
months without a period but she may continue to have symptoms a few
years past this time. Everyone's experience during this life transition
is unique. Women entering the stage of peri-menopause are in the process
of creation of a new life rhythm. This is why it is sometimes
uncomfortable. The more she is open to change, the better the outcome.
However, this does not mean the journey is easy. The outcome is better
if she is open to the challenges, discomforts and unexpected solutions.
The truth is a full investment in the journey guarantees discomfort.
This discomfort comes with an upside. It is an opportunity to establish
a deep knowing of self and a strong comfort with life. Our PhenomX
platform is for women who wish to know more about their personal
experience during this life transition and how to treat their health
symptoms using nutrition therapies for a healthy and empowered aging
experience.]{._2PHJq .public-DraftStyleDefault-ltr}

::: {type="paragraph" hook="rcv-block1"}
:::

::: {#viewer-ana4r .mm8Nw ._1j-51 ._1atvN ._1FoOD ._3M0Fe ._2WrB- ._1atvN .public-DraftStyleDefault-block-depth0 .fixed-tab-size .public-DraftStyleDefault-text-ltr}
[\
]{._2PHJq .public-DraftStyleDefault-ltr}
:::

::: {type="empty-line" hook="rcv-block2"}
:::

::: {#viewer-5e4t8 .mm8Nw ._1j-51 ._1atvN ._1FoOD ._3M0Fe ._2WrB- ._1atvN .public-DraftStyleDefault-block-depth0 .fixed-tab-size .public-DraftStyleDefault-text-ltr}
[\
]{._2PHJq .public-DraftStyleDefault-ltr}
:::

::: {type="empty-line" hook="rcv-block3"}
:::

::: {#viewer-47lgf ._2vd5k ._208vH}
::: {._3CWa- .dhpWm .dhpWm ._3mymk}
::: {._2kEVY hook="imageViewer" role="button" tabindex="0"}
::: {._3WJnn ._2Ybje}
![](file.jpg){.OzAYt ._3ii3f}

::: {.-D6i8}
:::
:::

<div>

</div>

<div>

</div>
:::
:::
:::

::: {type="image" hook="rcv-block5"}
:::

::: {#viewer-f8oj9 .mm8Nw ._1j-51 ._1atvN ._1FoOD ._3M0Fe ._2WrB- ._1atvN .public-DraftStyleDefault-block-depth0 .fixed-tab-size .public-DraftStyleDefault-text-ltr}
[\
]{._2PHJq .public-DraftStyleDefault-ltr}
:::

::: {type="empty-line" hook="rcv-block6"}
:::

::: {#viewer-f99sm .mm8Nw ._1j-51 ._1atvN ._1FoOD ._3M0Fe ._2WrB- ._1atvN .public-DraftStyleDefault-block-depth0 .fixed-tab-size .public-DraftStyleDefault-text-ltr}
[\
]{._2PHJq .public-DraftStyleDefault-ltr}
:::

::: {type="empty-line" hook="rcv-block7"}
:::

::: {type="last" hook="rcv-block-last"}
:::
:::
:::
:::
:::
:::
:::
:::

::: {#post-footer ._1AZWZ .mxwvP}
::: {._2MaCJ hook="post-main-actions-desktop"}
::: {._3A4UZ .blog-separator-background-color}
:::

::: {._3HCJ3 ._2oeUd}
::: {._1J-gM}
::: {#post-social-actions_61dad5ee5a03780016aa534e ._1XNTM .blog-text-color tabindex="-1"}
[]{._38Zqt}

[]{._38Zqt}

[]{._38Zqt}

[]{._38Zqt}
:::
:::
:::

::: {._3A4UZ .blog-separator-background-color}
:::

::: {._3HCJ3 ._1TzVu hook="post-main-actions__stats"}
::: {.g7Wot hook="post-stats"}
[0 views]{tabindex="0"}[0 comments]{tabindex="0"}
:::

[Post not marked as liked]{._3KwtW aria-live="off"}[]{._1l1q9
hook="like-button-with-count__like-count"}

::: {.like-button ._18fcf hook="like-button"}
::: {.tphzA}
::: {._2XlvE}
:::
:::

::: {.BEhyu}
::: {._2XlvE}
:::
:::

::: {._2jaiT}
::: {._2XlvE}
:::
:::

::: {.rCBA_}
::: {._2XlvE}
:::
:::

::: {._1F9Kk}
:::
:::
:::
:::
:::
:::
:::

</div>

::: {._1-0Rh}
<div>

::: {._2-8tD}
::: {._1_m3M}
[]{._1jqCz .blog-text-background-color}[]{._1jqCz
.blog-text-background-color}[]{._1jqCz .blog-text-background-color}
:::
:::

</div>

<div>

</div>
:::

<div>

</div>

</div>

</div>
:::
:::
:::

<div>

</div>
:::
:::

</div>
:::
:::

::: {#adi_page1007_1_117 .qhwIj .ignore-focus tabindex="-1" role="region" aria-label="Post: Blog2_Post"}
 
:::
:::
:::
:::
:::
:::
:::
:::

::: {#SITE_FOOTER .ZW5SX}
::: {._2o25M}
::: {._1nRb9}
:::
:::

::: {._38XI2}
::: {._2P6JN}
::: {._1dlfY}
:::
:::

::: {._1_UPn}
::: {mesh-id="SITE_FOOTERinlineContent" testid="inline-content"}
::: {mesh-id="SITE_FOOTERinlineContent-gridContainer" testid="mesh-container-content"}
::: {#comp-ky9xce9d ._11gHK}
::: {mesh-id="comp-ky9xce9dinlineContent" testid="inline-content"}
::: {mesh-id="comp-ky9xce9dinlineContent-gridContainer" testid="mesh-container-content"}
::: {mesh-id="comp-ky9xceaginlineContent" testid="inline-content"}
::: {mesh-id="comp-ky9xceaginlineContent-gridContainer" testid="mesh-container-content"}
::: {#comp-ky9xceb2 ._2Hij5 testid="richTextElement"}
[[[Sign up for PhenomX Updates]{.color_11}]{style="font-size:22px;"}]{style="letter-spacing:normal;"} {#sign-up-for-phenomx-updates .font_0 style="line-height:normal; text-align:center; font-size:22px;"}
=====================================================================================================
:::

::: {#comp-ky9xcebb ._2dBhC ._65cjg}
::: {.XRJUI}
:::
:::

::: {#comp-ky9xcebu ._2UgQw aria-disabled="false"}
[Submit]{._1Qjd7}
:::

::: {#comp-ky9xcec7 ._1Q9if testid="richTextElement"}
[Thanks for submitting!]{.color_11}
:::
:::
:::
:::
:::
:::

::: {#comp-ky74brex1 ._1Q9if testid="richTextElement"}
[+41 79 866 82 55]{.color_11}
:::

::: {#comp-ky74brey ._2O-Ry}
-   [[![Facebook](Facebook.png)](https://www.facebook.com/wix){._26AQd}]{#dataItem-ky9xcc52-comp-ky74brey}
-   [[![Twitter](Twitter.png)](https://twitter.com/Wix){._26AQd}]{#dataItem-ky9xcc59-comp-ky74brey}
-   [[![LinkedIn](LinkedIn.png)](https://www.linkedin.com/company/wix-com){._26AQd}]{#dataItem-ky9xcc5a1-comp-ky74brey}
:::

::: {#comp-ky74brey1 ._1Q9if testid="richTextElement"}
[©2022 by PhenomX Health. Proudly created with Wix.com]{.color_11}
:::
:::
:::
:::
:::
:::
:::
:::

::: {#SCROLL_TO_BOTTOM .qhwIj .ignore-focus tabindex="-1" role="region" aria-label="bottom of page"}
 
:::
:::
:::
